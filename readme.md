# MetalTxus' Uncertainty

## Description

**MetalTxus' Uncertainty** is a minimalist resource pack for those who dislike complex and overwhelming ones, designed to be beautiful yet simple. Any computer should be able to handle it at full speed and let the player enjoy its beauty.  
I hope you like it as much as I enjoyed developing it ;)

I add stuff progressively and only release new builds when "enough" features
cumulate, usually when most or all features for the current version are
finished. You can, however, download the
[latest development build](https://bitbucket.org/MetalTxus/metaltxus-uncertainty-resource-pack-for-minecraft/get/develop.zip)
in between releases to get early access to these features.

Feedback would be highly appreciated and any kind of suggestion is welcome. If there is something looking kind of weird or some texture you would like changed you can tell me. If your feedback sounds reasonable, I'll be glad to implement it.

## Links

- [Minecraftforum post](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/1245339-1-16-x-32x-metaltxus-uncertainty)
- [PlanetMinecraft post](https://www.planetminecraft.com/texture_pack/metaltxus-uncertainty/)

[![btn_donate_LG.gif](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/donate/?hosted_button_id=ACRQC8X7FT6AU)

## TODO

- remove "grass" or "short_grass", once Mojang settles on a name
- (block) crafter
- (block) sculk tendrils
- (block) sea vegetation
- (entity) hoglin
- (entity) warden

## Won't do

- (block) light
- (entity) horse armors
- (entity) llama clothes
- (entity) villager clothes
